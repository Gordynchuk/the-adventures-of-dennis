import groovy.io.FileType

def srcDirName = 'html_en'
def destDirName = 'html_en_proc'

def srcDir = new File(srcDirName)

println "Deleting $destDirName"
new AntBuilder().delete(dir: destDirName)

println "Copying book.html..."
new AntBuilder().copy(file: "$srcDirName/book.html", toFile: "$destDirName/book.html")
new AntBuilder().copy(toDir: "$destDirName/book_files") {
    fileset(dir: "$srcDirName/book_files", includes: "**")
}

def oldURLs = []
def newURLs = []

srcDir.eachFile FileType.FILES, {
    if (it.name.startsWith(".")) {
        return
    }

    if (it.name.endsWith("book.html")) {
        return
    }

    def savedFromStr = it.find {it.contains("<!-- saved from url")}
    assert savedFromStr

    def matcher = (savedFromStr =~ /<!-- saved from url=\(\d+\)(.*) -->/)
    assert matcher.matches()

    def oldURL = matcher.group(1)
    assert oldURL

    def fileNumber = oldURL.substring(oldURL.lastIndexOf("/") + 1, oldURL.lastIndexOf("."))
    def newURL = "$fileNumber-$it.name".replace(" ", "_")
    def newFileName = "$destDirName/$newURL"
    def oldFolderShortName = it.name.substring(0, it.name.lastIndexOf('.')) + '.files'
    def oldFolderName = "$srcDirName/$oldFolderShortName"
    def newFolderShortName = newURL.substring(0, newURL.lastIndexOf('.')) + '.files'
    def newFolderName = "$destDirName/$newFolderShortName"

    println "Copying ${it.name} to $newFileName..."
    new AntBuilder().copy(file: it, toFile: newFileName)
    println "Copying $oldFolderName to $newFolderName..."
    new AntBuilder().copy(toDir: newFolderName) {
        fileset(dir: oldFolderName, includes: "**")
    }

    println "Fixing references to resources..."
    def newFile = new File(newFileName)
    def pageText = newFile.text
    newFile.write(pageText.replace(oldFolderShortName, newFolderShortName))

    oldURLs.add(oldURL)
    newURLs.add(newURL)
}

println "Replacing cross-references between files..."
for (int i = 0; i < oldURLs.size(); i++) {
    // Replace references to the file
    new File(destDirName).eachFile FileType.FILES, {
        it.write(it.text.replace(oldURLs[i], newURLs[i]))
    }
}

println "Adding proper link to ToC..."
new File(destDirName).eachFile FileType.FILES, {
    it.write(it.text.replace("http://home.freeuk.net/russica2/books/den/book.html", "book.html"))
}

println "All done"